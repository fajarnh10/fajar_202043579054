package com.college;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class Fajar_9054_P33e {

	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		try {
			System.out.println("~UBAH DATA NILAI MAHASISWA~");
			System.out.print("Input NPM\t: "); String npm = scan.nextLine();
			ResultSet set = executeQueryMethod("SELECT * FROM mahasiswa WHERE NPM ='"+ npm +"'");
			viewDataModel1(set);
			updateData(npm);
			set = executeQueryMethod("SELECT * FROM mahasiswa WHERE NPM ='"+ npm +"'");
			viewDataModel2(set);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	//method comunination into DB to execution query request
	public static ResultSet executeQueryMethod(String query) {
		ResultSet set = null;
		try {
			Class.forName("org.sqlite.JDBC");
			Connection con 
			= DriverManager.getConnection("JDBC:sqlite:D:/database/FajarNurHidayat.db");
			Statement stat = con.createStatement();
			
			if (query.toUpperCase().contains("UPDATE")) {
				stat.executeUpdate(query);
			}else {
				set = stat.executeQuery(query);
			}
		} catch (Exception e) {
			System.out.println("Error when get data : " + e);
		}
		
		return set;
	}
	
	//view update data
	public static void updateData(String npm) {
		try {
			System.out.println("-----------------------------------------------------------------------------------------------------");
			System.out.println("~Input perubahan data~");
			System.out.print("Nama\t\t : "); String nama = scan.nextLine();
			System.out.print("Kelas\t\t : "); String kelas = scan.nextLine();
			System.out.print("Nilai Teori\t : "); String teori = scan.nextLine();
			System.out.print("Nilai Praktik\t : "); String praktik = scan.nextLine();
			
			executeQueryMethod("UPDATE mahasiswa SET Nama='"+ nama +"', Kelas='"+ kelas +"', teori='"+ teori +"', praktikum='"+ praktik +"'");
		} catch (Exception e) {
			System.out.print(e);
		}
	}
	
	//view data after search mahasiswa by NPM from database
	public static void viewDataModel1(ResultSet set) {
		try {
			System.out.println("~Hasil pencarian data~");
			while (set.next()) {
				String ab = set.getString("Nama");
				System.out.println("\tNama\t\t : " + ab);
				
				String ac = set.getString("Kelas");
				System.out.println("\tKelas\t\t : " + ac);
				
				String ad = set.getString("teori");
				System.out.println("\tNilai Teori\t : " + ad);
				
				String ae = set.getString("praktikum");
				System.out.println("\tNilai Praktik\t : " + ae);
			}
		} catch (Exception e) {
			System.out.print(e);
		}
	}
	
	//view data after edit data
	public static void viewDataModel2(ResultSet set) {
		try {
			System.out.println("=====================================================================================================");
			System.out.println("NPM\t\tNama Mahasiswa\t\tKelas\tTeori\tPraktikum\tNilai Akhir\tKeterangan");
			System.out.println("-----------------------------------------------------------------------------------------------------");
			while (set.next()) {
				String a = set.getString("NPM");
				System.out.print(a);
				
				String ab = set.getString("Nama");
				System.out.print("\t" + ab);
				
				String ac = set.getString("Kelas");
				System.out.print("\t" + ac);
				
				String ad = set.getString("teori");
				System.out.print("\t" + ad);
				
				String ae = set.getString("praktikum");
				System.out.print("\t" + ae + "\t\t");
				
				float nilaiAkhir = (((Float.parseFloat(ad))*30)/100) + (((Float.parseFloat(ae))*70)/100);
				System.out.printf("%.2f", + nilaiAkhir);
				
				System.out.print((nilaiAkhir >= 70)?"\t\tLULUS":"\t\tTIDAK LULUS");
			}
		} catch (Exception e) {
			System.out.print(e);
		}
	}
}
