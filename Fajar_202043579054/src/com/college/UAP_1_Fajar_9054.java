package com.college;

import java.applet.*;
import java.awt.*;
import java.awt.geom.*;
public class UAP_1_Fajar_9054 extends Applet{
	public void paint(Graphics ga){ 
		 Graphics2D g = (Graphics2D)ga; 
		 g.draw(new RoundRectangle2D.Double(0,0,420,170,40,40));//frameborder
	 
		 int[] x = {25, 50, 75};
		 int[] y = {70, 30, 70};
		 g.drawPolygon(x,y,3);//segitiga atas
		 
		 int[] x1 = {25, 50, 75};
		 int[] y1 = {100, 140, 100};
		 g.drawPolygon(x1,y1,3);//segitiga bawah
		 
		 int[] x2 = {180, 210, 240};
		 int[] y2 = {110, 160, 110};
		 g.drawPolygon(x2,y2,3);//segitiga tengah
		 
		 g.draw(new Rectangle2D.Double(140,40,50,50));//kotak kiri
		 g.draw(new Ellipse2D.Double(150,50,30,30));//lingkaran kiri
		 g.draw(new Rectangle2D.Double(220,40,50,50));//kotak kanan
		 
		 g.draw(new Ellipse2D.Double(230,50,30,30));//lingkaran kanan
		 g.draw(new Arc2D.Double(330,10,80,80,30,80,Arc2D.OPEN));//arc kanan atas
		 g.draw(new Arc2D.Double(320,80,80,80,180,90,Arc2D.OPEN));//arc kanan bawah
		 g.draw(new Arc2D.Double(10,10,80,80,120,80,Arc2D.OPEN));//arc kiri atas
		 g.draw(new Arc2D.Double(20,80,80,80,300,80,Arc2D.OPEN));//arc kiri bawah
		 g.draw(new Ellipse2D.Double(330,30,50,50));//lingkaran atas
		 g.draw(new Ellipse2D.Double(330,100,50,50));//lingkaran bawah
		 
		 g.setStroke(new BasicStroke(4));  
		 g.draw(new Line2D.Double(210,30,210,90)); //garis tengah vertical
		 g.draw(new Line2D.Double(150,100,260,100)); //garis tengah horizonal
		 g.draw(new Line2D.Double(140,110,140,150)); //garis kiri vertical
		 g.draw(new Line2D.Double(270,110,270,150)); //garis kanan vertical
	 }
}