package com.college;

import java.applet.Applet;
import java.awt.*;
import java.awt.geom.*;
public class UAP_2_Fajar_9054 extends Applet{
	 public void paint(Graphics ga){ 
		 Graphics2D g = (Graphics2D)ga;  
		 Shape a = new Ellipse2D.Double(90,90,40,40);//bulat tengah
		 Area b = new Area(new Rectangle2D.Double(10,10,200,200)); //frame
		 Area c = new Area(new Ellipse2D.Double(20,20,180,180)); //bulat besar tengah
		 b.subtract(new Area(new Rectangle2D.Double(20,20,40,40))); //kotak kiri atas
		 b.subtract(new Area(new Rectangle2D.Double(170,160,30,30))); //kotak kanan bawah
		 b.subtract(new Area(new Ellipse2D.Double(160,20,40,40))); //bulan atas
		 b.subtract(new Area(new Ellipse2D.Double(20,160,40,40))); //bulat bawah
		 c.subtract(new Area(new Rectangle2D.Double(20,20,60,60))); //kotak kiri atas big 
		 c.subtract(new Area(new Rectangle2D.Double(150,140,50,50))); //kotak kanan bawah
		 c.subtract(new Area(new Ellipse2D.Double(150,10,60,60))); //bulan atas big
		 c.subtract(new Area(new Ellipse2D.Double(10,150,60,60))); //bulat bawah big
		 b.subtract(c); 
		 g.draw(b);
		 g.draw(a); 
	 }
}