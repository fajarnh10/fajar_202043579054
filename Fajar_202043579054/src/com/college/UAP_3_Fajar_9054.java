package com.college;
import java.awt.*;
import java.awt.geom.*;
public class UAP_3_Fajar_9054 extends java.applet.Applet{
public void paint(Graphics g){
Graphics2D g2 = (Graphics2D)g;
Shape s1 = new Ellipse2D.Double(20,0,60,60);
Shape s2 = new Ellipse2D.Double(140,0,60,60);
Shape s3 = new Ellipse2D.Double(60,40,100,100);
Shape s4 = new Ellipse2D.Double(20,120,60,60);
Shape s5 = new Ellipse2D.Double(140,120,60,60);
Shape s6 = new Rectangle2D.Double(80,60,60,60);
Shape s7 = new Rectangle2D.Double(10,20,200,20);
Shape s8 = new Rectangle2D.Double(10,140,200,20);
Area a1 = new Area(s1); Area a2 = new Area(s2);
Area a3 = new Area(s3); Area a4 = new Area(s4);
Area a5 = new Area(s5); Area a6 = new Area(s6);
Area a7 = new Area(s7); Area a8 = new Area(s8);
a3.exclusiveOr(a1); a3.exclusiveOr(a2);
a3.exclusiveOr(a4); a3.exclusiveOr(a5);
a3.subtract(a6); a3.exclusiveOr(a7);
a3.exclusiveOr(a8);
g2.fill(a3);
}
}