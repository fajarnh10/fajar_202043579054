package com.college;

import java.awt.Graphics;
import java.applet.Applet;
import java.awt.Color;

public class GambarAndro extends Applet {
    public void paint (Graphics g){
    	 //kepala
    	g.setColor(Color.green);
        g.fillArc(60,30,60,80,0,180);
        g.drawLine(60,10,80,40);
        g.drawLine(120,10,100,40);
        g.setColor(Color.black);
        g.fillArc(100, 45, 10 ,10,0,360);
        g.drawOval(100, 45, 10 ,10);
        g.fillArc(70, 45, 10 ,10,0,360);
        g.drawOval(70, 45, 10 ,10);
        
        g.setColor(Color.green);
        g.fillRect(50,80,80,100);
        g.fillOval(20, 80, 20 ,90);
        g.fillOval(140, 80, 20 ,90);
        
        g.fillRect(55,180,25,80);
        g.fillRect(100,180,25,80);
        
        g.fillOval(45, 260, 45 ,20);
        g.fillOval(90, 260, 45 ,20);
        
        
    }
}
