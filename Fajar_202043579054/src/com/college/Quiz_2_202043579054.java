package com.college;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

public class Quiz_2_202043579054 extends Applet {

	public void paint(Graphics g) {
		g.setColor(Color.blue);
		
		//atas, kiri, bawah, kanan
        int x[] = { 100,20,100,180};
        int y[] = { 50,100,150,100};
        g.fillPolygon(new Polygon(x, y, 4));
        
        g.setColor(Color.black);
        int x1[] = { 100,70,100,130};
        int y1[] = { 60,80,100,80};
        g.fillPolygon(new Polygon(x1, y1, 4));
        
        g.setColor(Color.yellow);
        int xYellow[] = { 100,80,100,120};
        int yYellow[] = { 140,125,110,125};
        g.fillPolygon(new Polygon(xYellow, yYellow, 4));
        
        g.setColor(Color.green);
        int xGreen[] = { 65,40,70,95};
        int yGreen[] = { 85,100,120,105};
        g.fillPolygon(new Polygon(xGreen, yGreen, 4));
        
        g.setColor(Color.red);
        int xRed[] = { 135,105,135,165};
        int yRed[] = { 85,105,120,100};
        g.fillPolygon(new Polygon(xRed, yRed, 4));
        
        g.setColor(Color.red);
        g.fillOval(200,50,100,100);
        
        g.setColor(Color.yellow);
        g.drawRoundRect(350,50,200,100,10,10);
        
        int xtl[] = { 350,550,450 };
        int ytl[] = { 50,50,100 };
        g.setColor(Color.blue);
        g.fillPolygon(new Polygon(xtl, ytl, 3));
        
        int xt2[] = { 350,550,450 };
        int yt2[] = { 150,150,100 };
        g.setColor(Color.red);
        g.fillPolygon(new Polygon(xt2, yt2, 3));
        
        int xt3[] = { 350,350,450 };
        int yt3[] = { 50,150,100 };
        g.setColor(Color.yellow);
        g.fillPolygon(new Polygon(xt3, yt3, 3));
        
        int xt4[] = { 550,550,450 };
        int yt4[] = { 50,150,100 };
        g.setColor(Color.yellow);
        g.fillPolygon(new Polygon(xt4, yt4, 3));
	}
}
