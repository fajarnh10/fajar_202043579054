package com.college;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

public class Panah extends Applet {

	public void paint(Graphics g) {
		//Square
        int xs[] = { 20,20,520,520 };
        int ys[] = { 20,520,520,20 };
        g.drawPolygon(xs,ys,4);
        
        //Main triangel
        for (int i = 0; i < 6; i++) {
            int xt[] = { 20,20,520 - (80*i) };
            int yt[] = { 20,520,270 };
        	g.drawPolygon(xt,yt,3);
		}
        
        //Little triangel
        g.drawLine(420, 60, 470, 60);
        int xtl[] = { 470,470,500 };
        int ytl[] = { 50,70,60 };
        g.setColor(Color.black);
        g.fillPolygon(new Polygon(xtl, ytl, 3));
        
	}
}
