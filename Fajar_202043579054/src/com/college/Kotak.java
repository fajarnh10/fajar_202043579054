package com.college;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class Kotak extends Applet{

	public void paint(Graphics g) {
		
		Color colorList[] = {Color.BLUE, Color.GRAY, Color.BLACK, Color.PINK, Color.ORANGE, Color.CYAN, Color.YELLOW, Color.RED };  
		
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				g.setColor(colorList[j]);
				g.fillRect(20 + (90*j),20 + (80*i),60,60);
			}
			
			Color colorListSub[] = new Color[colorList.length];
			colorListSub[0] = colorList[colorList.length - 1];
			for (int j = 0; j < colorList.length - 1; j++) {
				colorListSub[j + 1] = colorList[j];
			}
			colorList = new Color[colorListSub.length];
			colorList = colorListSub;
		}
    }
	
}
