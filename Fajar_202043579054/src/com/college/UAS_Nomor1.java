package com.college;

import java.applet.Applet;
import java.awt.*;

public class UAS_Nomor1 extends Applet{
	public void paint(Graphics g) {
		g.drawRoundRect(200, 95, 90, 90, 90, 90);
		g.drawRoundRect(70, 70, 350, 175, 60, 60);
		
		int x1[] = {180,130,80,80,130,180};
		int y1[] = {145,95,145,180,130,180};
		int pts = x1.length;
		g.drawPolygon(x1, y1, pts);
		
		int x2[] = {410,360,310,310,360,410};
		int y2[] = {145,95,145,180,130,180};
		int pts2 = x2.length;
		g.drawPolygon(x2, y2, pts2);
		
		g.drawRect(183, 85, 10, 110);
		g.drawRect(297, 85, 10, 110);
		g.drawRect(80, 215, 330, 10);
	}
}
