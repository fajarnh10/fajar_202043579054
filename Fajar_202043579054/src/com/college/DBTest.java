package com.college;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class DBTest {
	public static void main(String[] args) {
		menu();
	}
	
	public static void menu() {
		try {
			Scanner scan = new Scanner(System.in);
			int noMenu = 0;
			System.out.print("\n1. Show Data\n2. Insert Data"
					+ "\n3. Delete Data\nSilahkan pilih menu yang di atas : ");
			noMenu = scan.nextInt();
			
			if (noMenu != 0) {
				if (noMenu == 1) {
					getData();
					menu();
				}else if (noMenu == 2) {
					insertData();
					menu();
				}else if (noMenu == 3) {
					deleteData();
					menu();
				}else {
					System.out.print("Anda memilih nomor "
							+ "["+ noMenu +"], tidak terdaftar !");
					menu();
				}
			}else {
				System.out.println("Silahkan pilih menu !");
				menu();
			}
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public static void getData() {
		ResultSet set = null;
		try {
			Class.forName("org.sqlite.JDBC");
			Connection con 
			= DriverManager.getConnection("JDBC:sqlite:D:/database/kampus.db");
			Statement stat = con.createStatement();
			set = stat.executeQuery("select * from tb_barang");
			System.out.println("Kode\t\tNama Barang\t\tHarga");
			System.out.println("===================================================");
			while (set.next()) {
				String a = set.getString("Kode_barang");
				System.out.print(a);
				
				String ab = set.getString("Nama_barang");
				System.out.print("\t\t" + ab);
				
				String ac = set.getString("harga");
				System.out.println("\t\t\t" + ac);
			}
		} catch (Exception e) {
			System.out.println("Error when get data : " + e);
		}
	}
	
	public static void insertData() {
		ResultSet set = null;
		try {
			Class.forName("org.sqlite.JDBC");
			Connection con 
			= DriverManager.getConnection("JDBC:sqlite:D:/database/kampus.db");
			Statement stat = con.createStatement();
			Scanner scan = new Scanner(System.in);
			
			System.out.println("Tambah Record...");
			System.out.print("Kode Barang : ");
			String kb = scan.nextLine();
			System.out.print("Nama Barang : ");
			String nb = scan.nextLine();
			System.out.print("Harga : ");
			int hb = scan.nextInt();
			
			stat.executeUpdate("INSERT INTO tb_barang VALUES ('"+ kb +"','"+ nb +"','',"+ hb +")");
			getData();
		} catch (Exception e) {
			System.out.println("Error when get data : " + e);
		}
	}
	
	public static void deleteData() {
		ResultSet set = null;
		try {
			Class.forName("org.sqlite.JDBC");
			Connection 
			con = DriverManager.getConnection("JDBC:sqlite:D:/database/kampus.db");
			Statement stat = con.createStatement();
			Scanner scan = new Scanner(System.in);
			
			System.out.println("Delete Record...");
			System.out.print("Kode Barang : ");
			String kb = scan.nextLine();
			
			stat.executeUpdate("DELETE FROM tb_barang WHERE Kode_barang = '"+ kb +"'");
			getData();
		} catch (Exception e) {
			System.out.println("Error when get data : " + e);
		} 
	}
}
