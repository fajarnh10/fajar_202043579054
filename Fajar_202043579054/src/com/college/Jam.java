package com.college;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class Jam extends Applet {

	public void paint(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillOval(10,10,220,220);
		g.drawOval(10,10,220,220);
		g.setColor(Color.YELLOW);
		g.fillOval(20,20,200,200);
		g.drawOval(20,20,200,200);
		g.setColor(Color.BLUE);
		g.fillOval(115, 115, 10, 10);
		g.drawOval(115, 115, 10, 10);
		g.setColor(Color.BLACK);
		g.drawLine(50, 75, 120, 120);
		g.drawLine(125, 180, 120, 120);
		g.setColor(Color.GRAY);
		g.drawLine(120, 120, 200, 120);
		g.setColor(Color.RED);
		g.drawLine(170, 45, 170, 65);
	}
	
}
