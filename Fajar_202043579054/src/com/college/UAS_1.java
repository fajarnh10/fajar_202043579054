package com.college;

import java.applet.Applet;
import java.awt.Graphics;

public class UAS_1 extends Applet{
	public void paint(Graphics g) {
		g.drawRoundRect(20,20,320,150,50,50);
		g.drawRect(30, 150, 300, 10);
		g.drawRect(115, 40, 10, 100);
		g.drawRect(235, 40, 10, 100);
		g.drawOval(140, 50, 80, 80);
		 
		int x1[] = {30,30,70,110,110,70};
		int y1[] = {130,90,50,90,130,90};
		g.drawPolygon(x1, y1, x1.length);
		 
		int x2[] = {250,250,290,330,330,290};
		int y2[] = {130,90,50,90,130,90};
		g.drawPolygon(x2, y2, x2.length);
	}
}
