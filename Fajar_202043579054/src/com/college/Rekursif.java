package com.college;

import java.util.Scanner;

public class Rekursif {
	static String a = "${change}";
	public static void main(String[] args) {
		Scanner abc = new Scanner(System.in);
		System.out.print("Digit terakhir NPM anda : ");
		int npm = abc.nextInt();
		System.out.println(npm+"\t"+pagi(npm));
		System.out.println(a.replace("${change}", String.valueOf(npm)));
	}
	
	static int pagi(int b) {
		
		System.out.println(b);
		if (b == 0) {
			a = "2*pagi(" + a + ")-1";
			System.out.println(a.replace("${change}", String.valueOf(b)));
			return 3;
		}else {
			a = "2*pagi(" + a + ")-1";
			System.out.println(a.replace("${change}", String.valueOf(b-1)));
			return 2*pagi(b-1)-1;
		}
	}
	
}
