package com.college;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class Bintang extends Applet {

	public void paint(Graphics g) {
        
		g.setColor(Color.YELLOW);
		int[] x1 = {250,310,370};
        int[] y1 = {210,30,210};
        g.fillPolygon(x1,y1,3);
        
        int[] x2 = {40,310,580};
        int[] y2 = {210,400,210};
        g.fillPolygon(x2,y2,3);
       
        int[] x3 = {140,200,310};
        int[] y3 = {500,320,400};
        g.fillPolygon(x3,y3,3);
       
        int[] x4 = {310,420,480};
        int[] y4 = {400,320,500};
        g.fillPolygon(x4,y4,3);
	}
	
}
