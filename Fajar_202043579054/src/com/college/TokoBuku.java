package com.college;

import java.io.*;
import java.util.Scanner;
import java.sql.*;

public class TokoBuku {
	public static void main(String[] args) {
		try {
			BufferedReader f=new BufferedReader (new InputStreamReader (System. in));
			Connection kon;
			Statement s;
			ResultSet rs;
			
			Class.forName("org.sqlite.JDBC");
			kon=DriverManager.getConnection ("jdbc:sqlite:D:/database/latihan.db");
			s=kon.createStatement ();
			
			rs=s.executeQuery("select * from bukubuku;");
			System.out.println("Kode \t Judul Buku \t\t\t\t Kategori \t Tahun \t Harga");
			System.out.println ("===============================================================================");
			
			while (rs.next()){
				System.out.print (rs.getString ("kode") );
				System.out.print("\t " + rs.getString("judul"));
				System.out.print("\t "+rs.getString("kategori"));
				System.out.print("\t "+rs.getInt ("tahun") );
				System.out.println("\t Rp "+rs.getInt ("harga"));
			}
			
			System.out.println("Menu :");
			System.out.println(	"[1] Menampilkan Data \n"+
								"[2] Menambah Data \n"+
								"[3] Merubah Data \n"+
								"[4] Menghapus Data\n"+
								"[5] Transaksi");
			String m="t";
			System.out.print("\t >> Masuk ke Menu[y/t]:");
			m = f.readLine();
			
			while (m.equalsIgnoreCase("y")) {
				System.out.print("Pilihan : ");
				int pilih=Integer.parseInt (f.readLine());
				switch (pilih) {
					case 1:
						rs=s.executeQuery("select * from bukubuku;");
						System.out.println("Kode \t Judul Buku \t\t Kategori \t Tahun \t Harga");
						System.out.println ("===============================================================================");
						while (rs.next()){
								System.out.print (rs.getString ("kode") );
								System.out.print("\t "+rs.getString("judul"));
								System.out.print("\t "+rs.getString("kategori"));
								System.out.print("\t "+rs.getInt ("tahun") );
								System.out.println("\t Rp "+rs.getInt("harga"));
						}
						System.out.print("\t >> Kembali ke Menu? [y/t] : ");
						m=f.readLine();
						break;
					case 2: System.out.println("\t\t Menambah Data....");
						System.out.print ("Kode Buku\t : "); String a=f.readLine();
						System.out.print("Judul Buku\t : "); String b=f.readLine();
						System.out.print ("Kategori \t : "); String c=f.readLine();
						System.out.print("Tahun \t\t : "); int d=Integer.parseInt (f.readLine());
						System.out.print("Harga Buku\t : "); int e=Integer.parseInt (f.readLine());
						
						s.executeUpdate ("insert into bukubuku values ('" + a +"','" + b +"','" + c +"'," + d +"," + e +");");
						
						System.out.print("\t >> Kembali ke Menu? [y/t] : ");
						m=f.readLine();
						break;
					case 3: System.out.println("\t\t Merubah Data...");
							System.out.print ("Kode Buku\t : "); String ko=f.readLine();
							System.out.print("Judul Buku\t : "); String ju=f.readLine();
							System.out.print ("Kategori \t : "); String ka=f.readLine();
							System.out.print("Tahun \t\t : "); int ta=Integer.parseInt (f.readLine());
							System.out.print("Harga Buku\t : "); int ha=Integer.parseInt (f.readLine());
							
							s.executeUpdate ("update bukubuku set judul='"+ju+"',kategori='"+ ka +"', tahun="+ta+", harga="+ha+" where kode='"+ ko +"';");
							
							System.out.print("\t >> Kembali ke Menu? [y/t] : ");
							m=f.readLine();
							break;
					case 4: System.out.println("\t\t Menghapus Data....");
							System.out.print ("Kode Buku yang akan dihapus : "); String k=f.readLine();
							
							s.executeUpdate ("delete from bukubuku where kode='"+ k +"';");
							
							System.out.print("\t >> Kembali ke Menu? [y/t] : ");
							m=f.readLine();
							break;
					case 5: System.out.println("\t\t Transaksi Penjualan Buku....");
							System.out.print ("Kode Buku : "); String kd=f.readLine();
							ResultSet rsl=s.executeQuery("select * from bukubuku where kode='"+kd+"';");
							int hg=rs.getInt ("harga");
							while (rsl.next()){
								System.out.println("\t Judul Buku \t : " + rs.getString("judul"));
								System.out.println("\t Kategori \t : "+rs.getString("kategori"));
								System.out.println("\t Tahun \t\t : Rp "+rs.getInt("tahun"));
								System.out.println("\t Harga Buku\t : Rp " + rs.getInt("harga"));
							}
							System.out.print("Jumlah Beli : "); int jb=Integer.parseInt (f.readLine());
							int tot=jb*hg;
							System.out.println("------------------------------------------------------");
							System.out.println("\tTotal Bayar : "+tot);
					
							System.out.print("\t >> Kembali ke Menu? [y/t] : ");
							m=f.readLine();
							break;
						default:System.out.println("\t\t tidak ada dalam pilihan..");
						System.out.print(" >> Terimakasih.. << ");
				}
			}
			System.out.print("  --------  >> Terimakasih..  <<  -------- ");
		} catch (Exception e) {
			System.err.println("Error : "+e.getMessage()); 
		}
	}
}
