package com.college;
import java.awt.*;
import javax.swing.*;
public class UAS_2 extends JComponent{
	public void paint(Graphics g) {
		//Membuat wadah balok rounded
		g.setColor(Color.blue);
		g.fillRoundRect(20,20,450,150,50,50);
		//Mebuat segitiga kuning paling kiri
		g.setColor(Color.yellow);
        int x1[] = { 40,120,180};
        int y1[] = { 170,95,170};
        g.fillPolygon(new Polygon(x1, y1, 3));
        //membuat bidang hijau yg memiliki 7 sudut
        g.setColor(Color.green);
        int x2[] = {190,120,180,300,300,240};
        int y2[] = {20,95,170,170,20,95};
        g.fillPolygon(new Polygon(x2, y2, 6));
        //membuat dua segitiga paling kanan balok
        g.setColor(Color.yellow);
        int x3[] = { 300,300,400,470,470};
        int y3[] = { 170,20,95,150,40};
        g.fillPolygon(new Polygon(x3, y3, 5));
        //mmbuat lingkatang merah
        g.setColor(Color.red);
		g.fillOval(210, 120, 40, 40);
		//membuat lingkaran hijau
        g.setColor(Color.green);
		g.fillOval(60, 40, 110, 110);
		//segitiga merah dalam lingkaran
		g.setColor(Color.red);
		g.fillArc(60,40,110,110,45,180);
		//segitiga kuninga dalam lingkaran
		g.setColor(Color.yellow);
		g.fillArc(60,40,110,110,45,-93);
	}
	public static void main(String[] args) {
		//deklarasi class jFrame
		JFrame window = new JFrame();
		//Mengatur ukuran fram 500x500
        window.setSize(500, 500);
        //sets the JFrame visible
        window.setVisible(true);    
        //deklarasi class
        UAS_2 drawing = new UAS_2();    
        //Menambahkan method paint pada frame
        window.add(drawing);    
    }
}