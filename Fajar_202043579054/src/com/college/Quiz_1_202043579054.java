package com.college;

import java.applet.Applet;
import java.awt.Graphics;

public class Quiz_1_202043579054 extends Applet {

	public void paint(Graphics g) {
	
		int xd[] = {20, 220, 120};
		int yd1[] = {250, 250, 70};
		int yd2[] = {300, 300, 120};
		g.drawPolygon(xd, yd1, 3);
		g.drawPolygon(xd, yd2, 3);
		
		int xd1[] = {300, 500, 400};
		g.drawPolygon(xd1, yd1, 3);
		g.drawPolygon(xd1, yd2, 3);
		
		int xd2[] = {580, 780, 680};
		g.drawPolygon(xd2, yd1, 3);
		g.drawPolygon(xd2, yd2, 3);
		
	}
	
}
