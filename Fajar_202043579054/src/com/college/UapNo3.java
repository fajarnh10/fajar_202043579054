package com.college;

import java.awt.*;
public class UapNo3 extends java.applet.Applet{
	public void paint(Graphics g) {
		int r=30, e=45, h=65, t=33;
		g.fillOval(185, 37, 88, 88);
		g.fillOval(255, 37, 88, 88);
		
		int x[] = {193,260,337};
		int y[] = {107,200,103};
		g.fillPolygon(x, y, x.length);
		
		for(int i=0;i<56;i++){
			t=i%4;
			g.drawRect(r,e+t*h,43,56);
			g.drawOval(183,135,65,65);
			g.drawLine(73,e+t*h,216,166);
			g.drawLine(73,e+56+t*h,216,166);
		}
	}
}
